import { VoteComponent } from "./vote.component";

describe("VoteComponent", () => {
  let component = new VoteComponent();

  // Inicializar
  beforeEach(() => {
    component = new VoteComponent();
  });

  afterEach(() => {});

  beforeAll(() => {});

  afterEach(() => {});

  it("should increment amount of votes", () => {
    component.upVote();
    expect(component.totalVotes).toBe(1);
  });

  it("should decrement amount of votes", () => {
    component.downVote();
    expect(component.totalVotes).toBe(-1);
  });
});
