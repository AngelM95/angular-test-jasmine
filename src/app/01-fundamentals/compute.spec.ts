import { compute } from "./compute";

describe("compute", () => {
  it("should return 0 if input is negative", () => {
    const result = compute(-1);
    expect(result).toBe(0);
  });

  it("should increment intut if is possitive", () => {
    const result = compute(5);
    expect(result).toBe(6);
  });
});
