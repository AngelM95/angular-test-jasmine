import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable, of } from "rxjs";

export class TodoService {
  constructor(private http: HttpClient) {}

  add(todo): Observable<any> {
    return of(todo);
  }

  getTodos(): Observable<any> {
    return of([
      { id: 1, title: "a" },
      { id: 2, title: "b" },
      { id: 3, title: "c" }
    ]);
  }

  delete(id): Observable<any> {
    return of({ id: id });
  }
}
