import { TodosComponent } from "./todos.component";
import { TodoService } from "./todo.service";
import { of, throwError } from "rxjs";

describe("VoteComponent", () => {
  var component: TodosComponent;
  var service: TodoService;

  beforeEach(() => {
    service = new TodoService(null);
    component = new TodosComponent(service);
  });

  // onInit
  it("should retrive todos calling getTodos on init component and set todos property", () => {
    const todosFake = [
      { id: 1, title: "a" },
      { id: 2, title: "b" },
      { id: 3, title: "c" }
    ];
    spyOn(service, "getTodos").and.returnValue(of(todosFake));
    component.ngOnInit();
    expect(component.todos).toBe(todosFake);
  });

  // add
  it("should call to add server service on exec add() method", () => {
    let todoFakeToAdd = { id: 3, title: "d" };
    let spyToBeCalled = spyOn(service, "add").and.callFake(() => {
      return of(todoFakeToAdd);
    });
    component.add();
    expect(spyToBeCalled).toHaveBeenCalled();
  });

  it("should add new todo to todo property", () => {
    let numTodos = component.todos.length;
    let todoFakeToAdd = { id: 3, title: "d" };
    spyOn(service, "add").and.callFake(() => {
      return of(todoFakeToAdd);
    });
    component.add();
    expect(component.todos.length).toBe(numTodos + 1);
  });

  it("should change message property on get error response on addTodos", () => {
    let errorMessageMock = "Problema al añadir todo";
    spyOn(service, "add").and.returnValue(throwError(errorMessageMock));
    component.add();
    expect(component.message).toBe(errorMessageMock);
  });

  // delete
  it("should call to delete server service on exec delete() method on confirm", () => {
    let idTodoMock = 1;
    spyOn(window, "confirm").and.returnValue(true);
    let spyToBeCalled = spyOn(service, "delete").and.returnValue(of());
    component.delete(idTodoMock);
    expect(spyToBeCalled).toHaveBeenCalledWith(idTodoMock);
  });

  it("should NOT call to delete server service on exec delete() method on cancels", () => {
    let idTodoMock = 1;
    spyOn(window, "confirm").and.returnValue(false);
    let spyToBeCalled = spyOn(service, "delete").and.returnValue(of());
    component.delete(idTodoMock);
    expect(spyToBeCalled).not.toHaveBeenCalledWith(idTodoMock);
  });
});
